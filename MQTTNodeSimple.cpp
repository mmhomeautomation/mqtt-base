#include <Arduino.h>
#include <MQTTClient.h>

#include "MQTTNodeSimple.hpp"
#include "MQTTBase.hpp"

MQTTNodeSimple::MQTTNodeSimple() {
	this->channelName = "";
	this->value = "";
	this->base = NULL;
}

MQTTNodeSimple::MQTTNodeSimple(String name) {
	this->channelName = name;
	this->value = "";
	this->base = NULL;
}

String MQTTNodeSimple::getName() {
	return channelName;
}

String MQTTNodeSimple::getValue() {
	return value;
}

void MQTTNodeSimple::setValue(String val) {
	value = val;
}

void MQTTNodeSimple::setValue(String val, bool publish) {
	MQTTNodeSimple::setValue(val);
	if(publish) MQTTNodeSimple::publishValue();
}

void MQTTNodeSimple::timer10ms() {
}

void MQTTNodeSimple::setBase(void * base) {
	this->base = base;
}

void MQTTNodeSimple::publishValue() {
	if(base) {
		( (MQTTBase*)base )->publishNodeValue(this);
	}
}
