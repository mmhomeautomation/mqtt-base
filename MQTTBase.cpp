#include <Arduino.h>
#include <MQTTClient.h>
#include <LinkedList.h>
#ifdef __PLATFORM_ESP8266
#include <ESP8266WiFi.h>
#elif defined(__PLATFORM_ESP32)
#include <WiFi.h>
#endif
#include <modules/mqtt-base/MQTTNodeSimple.hpp>

#include <modules/debug-utils/debug-utils.hpp>
#include <modules/debug-utils/Logger.hpp>

#include "MQTTBase.hpp"

MQTTBase *basePtr;

void messageReceived(String &topic, String &payload) {
	if(basePtr) { 
		basePtr->messageHandler(topic, payload);
	}
}

MQTTBase::MQTTBase(String server, int port, String baseTopic) {
	basePtr = this;

	this->wifiClient = new WiFiClient();
	this->mqttClient = new MQTTClient();
	channels = new LinkedList<MQTTNodeSimple*>();
	this->baseTopic = String(baseTopic);
	clientID = "";

	this->server = server;

	mqttClient->begin(this->server.c_str(), port, *wifiClient);
	mqttClient->onMessage(messageReceived);

	mqttState = MQTTBASE_DISCONNECTED;
}

void MQTTBase::timerISR() {
	for(uint8_t i = 0; i < channels->size(); i++) {
		MQTTNodeSimple * node = channels->get(i);
		node->timer10ms();
	}
}

void MQTTBase::connect(String clientID) {
	this->clientID = clientID;
	uint32_t last = millis();
	this->_connected = false;
}

void MQTTBase::setupAllNodes() {
	for(uint8_t i = 0; i < channels->size(); i++) {
		MQTTNodeSimple * node = channels->get(i);
		node->setupNode();
	}
}

void MQTTBase::loop() {
	if(!this->_connected) {
		if( mqttClient->connect( this->clientID.c_str() ) ) {
			this->_connected = true;
			mqttState = MQTTBASE_CONNECTED;
			_printf(" OK\n");
			if(!_wasConnected) {
				setupAllNodes();
			}
			_wasConnected = true;
			this->subscribeAll();
		}
	}else{
		mqttClient->loop();
	}

	if( this->_connected && !this->mqttClient->connected() ) {
		this->_connected = false;
		mqttState = MQTTBASE_DISCONNECTED;
	}
}

void MQTTBase::messageHandler(String topic, String payload) {
	logger->printf( "Handler called: '%s' = '%s'\n", topic.c_str(), payload.c_str() );

	for(uint8_t i = 0; i < channels->size(); i++) {
		MQTTNodeSimple * node = channels->get(i);
		String channelName = String( node->getName() );
		String channelTopic = baseTopic + "/" + channelName;

		/*
		 * setting parameter: write value in          base/set/<name>
		 * getting parameter: write parameter name in base/get
		 *                      -> parameter value is then updated in base/get/<name>
		 */

		if(topic.substring( 0, baseTopic.length() ) == baseTopic) {
			topic = topic.substring(baseTopic.length() + 1);

			int indexOfFirstSlash = topic.indexOf('/');
			enum MQTTRequestType_e type = (topic.substring(0, indexOfFirstSlash) == "get" ? MQTT_REQTYPE_GET : MQTT_REQTYPE_SET);

			if(type == MQTT_REQTYPE_GET) {
				if(indexOfFirstSlash == -1) {
					char * buffer = (char *)payload.c_str();
					char * next = strtok(buffer, ",");
					while(next != NULL) {
						//publishNodeValue(String(next));
						MQTTNodeSimple * node = findNodeByName( String(next) );
						if(node) {
							node->publishValue();
						}
						next = strtok(NULL, ",");
					}
				}
			} else {
				topic = topic.substring(indexOfFirstSlash + 1);
				MQTTNodeSimple * node = findNodeByName(topic);
				if(node) {
					node->setValue(payload, true);
				}
			}
		}
	}
}

void MQTTBase::subscribeAll() {
	String topic = baseTopic + "/";

	String stat = topic + "get";
	bool ok = mqttClient->subscribe( stat.c_str() );
	logger->printf("Subscription for global getter node, topic = '%s': %s\n", stat.c_str(), ok ? "OK" : "NOT OK");

	for(uint8_t i = 0; i < channels->size(); i++) {
		MQTTNodeSimple * node = channels->get(i);
		String channelName = String( node->getName() );

		stat = topic + "set/" + channelName;

		ok = mqttClient->subscribe( stat.c_str() );
		logger->printf("Subscription for node '%s', topic = '%s': %s\n", channelName.c_str(), stat.c_str(), ok ? "OK" : "NOT OK");
	}
}

MQTTNodeSimple * MQTTBase::addNode(String name) {
	MQTTNodeSimple * node = new MQTTNodeSimple(name);
	return addNode(node);
}

MQTTNodeSimple * MQTTBase::addNode(MQTTNodeSimple * node) {
	if( !findNodeByName( node->getName() ) ) {
		logger->printf( "Add node '%s'\n", node->getName().c_str() );
		node->setBase(this);
		channels->add(node);
		return node;
	}

	return NULL;
}

MQTTNodeSimple * MQTTBase::findNodeByName(String name) {
	for(uint16_t i = 0; i < channels->size(); i++) {
		MQTTNodeSimple * node = channels->get(i);
		if(node->getName() == name) {
			return node;
		}
	}

	return NULL;
}

void MQTTBase::setNodeValue(String name, String val, bool publish) {
	MQTTNodeSimple *node = findNodeByName(name);
	if(node) {
		node->setValue(val);
		logger->printf( "Set value for node '%s' to '%s'\n", node->getName().c_str(), node->getValue().c_str() );
		if(publish) node->publishValue();
	}
}

void MQTTBase::publishNodeValue(String name) {
	MQTTNodeSimple *node = findNodeByName(name);
	if(node) {
		node->publishValue();
	}
}

void MQTTBase::publishNodeValue(MQTTNodeSimple * node) {
	String topic = baseTopic + "/get/" + node->getName();
	logger->printf( "Publishing value for node '%s' (topic = '%s')\n", node->getName().c_str(), topic.c_str() );
	mqttClient->publish( topic.c_str(), node->getValue().c_str() );
}

String MQTTBase::getNodeValue(String name) {
	MQTTNodeSimple *node = findNodeByName(name);
	if(node) {
		return node->getValue();
	}
	return "";
}

bool MQTTBase::connected(void){
	return this->_connected;
}
