/*
 * MQTTNodeObservable.hpp
 *
 *  Created on: 05.01.2017
 *      Author: maddin
 */

#ifndef SRC_MODULES_MQTT_BASE_MQTTNODEOBSERVABLE_HPP_
#define SRC_MODULES_MQTT_BASE_MQTTNODEOBSERVABLE_HPP_

#include <LinkedList.h>
#include "MQTTNodeSimple.hpp"

class MQTTObserver {
public:
	MQTTObserver() {};

	virtual void update(String id, String newValue) = 0;
};

class MQTTNodeObservable: public MQTTNodeSimple {
	LinkedList<MQTTObserver *> observers;
	String id = "";

public:
	MQTTNodeObservable();
	MQTTNodeObservable(String name);

	void setID(String newID);
	void notifyObservers(void);
	void attachObserver(MQTTObserver * obs);
	void detachObserver(MQTTObserver * obs);
	void setValue(String val);
	void setValue(String val, bool publish);
};

#endif /* SRC_MODULES_MQTT_BASE_MQTTNODEOBSERVABLE_HPP_ */
