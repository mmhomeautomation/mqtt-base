/*
 * MQTTNodeObservable.cpp
 *
 *  Created on: 05.01.2017
 *      Author: maddin
 */

#include "MQTTNodeObservable.hpp"
#include "MQTTNodeSimple.hpp"

MQTTNodeObservable::MQTTNodeObservable() : MQTTNodeSimple() {};

MQTTNodeObservable::MQTTNodeObservable(String name) : MQTTNodeSimple(name) {
	observers.clear();
}

void MQTTNodeObservable::notifyObservers() {
	for(int i = 0; i < observers.size(); i++) {
		MQTTObserver * obs = observers.get(i);
		obs->update(id, getValue());
	}
}

void MQTTNodeObservable::setID(String newID) {
	id = newID;
}

void MQTTNodeObservable::setValue(String val) {
	MQTTNodeSimple::setValue(val);
	notifyObservers();
}

void MQTTNodeObservable::setValue(String val, bool publish) {
	MQTTNodeSimple::setValue(val, publish);
	notifyObservers();
}

void MQTTNodeObservable::attachObserver(MQTTObserver * obs) {
	observers.add(obs);
}

void MQTTNodeObservable::detachObserver(MQTTObserver * obs) {
	for(int i = 0; i < observers.size(); i++) {
		MQTTObserver * obsList = observers.get(i);
		if(obs == obsList) {
			observers.remove(i);
			return;
		}
	}
}
