#ifndef __MQTTNODESIMPLE_HPP__
#define __MQTTNODESIMPLE_HPP__

#include <Arduino.h>

class MQTTNodeSimple {
	String value;

protected:
	void * base;
	String channelName;

public:
	MQTTNodeSimple();
	MQTTNodeSimple(String name);

	String getName();
	void setBase(void * base);
	virtual String getValue();
	virtual void publishValue();
	virtual void setValue(String val);
	virtual void setValue(String val, bool publish);
	virtual void timer10ms();
	virtual void setupNode() {};
};

#endif
