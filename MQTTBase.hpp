#ifndef __MQTTBASE_HPP__
#define __MQTTBASE_HPP__

#include <Arduino.h>
#include <MQTTClient.h>
#include <LinkedList.h>
#ifdef __PLATFORM_ESP8266
#include <ESP8266WiFi.h>
#elif defined(__PLATFORM_ESP32)
#include <WiFi.h>
#endif
#include <modules/debug-utils/Logger.hpp>

#include "MQTTNodeSimple.hpp"

enum MQTTRequestType_e {
	MQTT_REQTYPE_GET = 0,
	MQTT_REQTYPE_SET = 1
};

enum MQTTBaseState_e {
	MQTTBASE_NOT_INITIALIZED,
	MQTTBASE_DISCONNECTED,
	MQTTBASE_CONNECTED
};

class MQTTBase {
	private:
		LinkedList<MQTTNodeSimple*> *channels = NULL;
		MQTTClient *mqttClient = NULL;
		WiFiClient *wifiClient = NULL;
		String baseTopic = "";
		String clientID = "";
		String server = "";
		ETSTimer timer;
		bool _connected = false;
		bool _wasConnected = false;
		enum MQTTBaseState_e mqttState = MQTTBASE_NOT_INITIALIZED;
		Logger *logger = new Logger("MQTTBase", 3);
		void setupAllNodes();

	public:
		MQTTBase() {
		};
		MQTTBase(String server, int port, String baseTopic);

		void loop();
		void connect(String clientID);
		void messageHandler(String topic, String payload);
		void subscribeAll();
		MQTTNodeSimple * addNode(String name);
		MQTTNodeSimple * addNode(MQTTNodeSimple * node);
		MQTTNodeSimple * findNodeByName(String name);
		void setNodeValue(String name, String val, bool publish);
		void publishNodeValue(String name);
		void publishNodeValue(MQTTNodeSimple * node);
		String getNodeValue(String name);
		void timerISR(void);
		bool connected(void);
};

#endif
